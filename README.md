# Ohjelmointinäyte

Projekti sisältää API:n, joka hakee valvottavat liikepankit Finanssivalvonnan sivustolta. Projekti on toteutettu Seleniumilla. 

Rajapinta on toteutettu REST-rajapintana, joka sisältää yhden GET-päätepisteen:
```
http://localhost:56848/api/fiva
```

Ensimmäinen suoritus on melko hidas Seleniumista johtuen, mutta seuraavat kutsut tulevat välimuistista, jonka kesto on asetettu 60 minuutiksi.