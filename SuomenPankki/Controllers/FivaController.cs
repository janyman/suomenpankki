﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using SuomenPankki.Services;

namespace SuomenPankki.Controllers
{
    [Route("api/[controller]")]
    public class FivaController : Controller
    {
        private IMemoryCache cache;
        private IFivaService fivaService;
        private IConfiguration config;

        public FivaController(IMemoryCache cache, IFivaService fivaService, IConfiguration config)
        {
            this.cache = cache;
            this.fivaService = fivaService;
            this.config = config;

            String path = System.Environment.GetEnvironmentVariable("PATH");

            // required by FirefoxDriver to find the geckodriver.exe
            System.Environment.SetEnvironmentVariable("PATH", ".;" + path);
        }

        // GET api/fiva
        [HttpGet]
        public IEnumerable<string> Get()
        {
            List<string> banks;
            cache.TryGetValue("banks", out banks);
            if (banks == null)
            {
                banks = fivaService.fetchBanks();
                var cacheExpiration = double.Parse(config["FivaConfig:cacheExpiration"]);
                var cacheEntryOptions = new MemoryCacheEntryOptions()
                    .SetAbsoluteExpiration(TimeSpan.FromMinutes(cacheExpiration));
                cache.Set("banks", banks);
            }

            return banks;
        }
    }
}
