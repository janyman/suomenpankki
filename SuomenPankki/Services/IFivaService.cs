﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SuomenPankki.Services
{
    public interface IFivaService
    {
        List<string> fetchBanks();
    }
}
