﻿using Microsoft.Extensions.Configuration;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SuomenPankki.Services
{
    public class FivaService : IFivaService
    {
        private IConfiguration config;

        public FivaService(IConfiguration config)
        {
            this.config = config;
        }

        private IWebElement getExpandElement(IWebDriver driver, String text)
        {
            string xpath = string.Format(config["FivaConfig:expandXPath"], text);
            return driver.FindElement(By.XPath(xpath));
        }

        private IWebElement getCheckboxElement(IWebDriver driver, String text)
        {
            string xpath = string.Format(config["FivaConfig:checkboxXPath"], text);
            return driver.FindElement(By.XPath(xpath));
        }

        private IWebElement getSubmitButton(IWebDriver driver, String text)
        {
            string xpath = string.Format(config["FivaConfig:submitXPath"], text);
            return driver.FindElement(By.XPath(xpath));
        }

        private List<string> getResults(IWebDriver driver)
        {
            string tableXPath = config["FivaConfig:tableXPath"];

            IWebElement tableElement =
                (
                    new WebDriverWait(driver, TimeSpan.FromSeconds(30))
                        .Until(ExpectedConditions.PresenceOfAllElementsLocatedBy(
                            By.XPath(tableXPath)))
                ).First();

            var textElements = tableElement.FindElements(By.XPath(config["FivaConfig:textXPath"]));

            var banks =
                (
                    from e in textElements
                    select e.Text
                ).ToList();

            return banks;
        }

        public List<string> fetchBanks()
        {
            IWebDriver driver = new FirefoxDriver();
            driver.Navigate().GoToUrl(config["FivaConfig:fivaUrl"]);

            getExpandElement(driver, "Luottomarkkinoilla toimivat yhteisöt")
                .Click();

            getExpandElement(driver, "Talletuspankit")
                .Click();

            getCheckboxElement(driver, "Liikepankit")
                .Click();

            getSubmitButton(driver, "Hae")
                .Click();

            var banks = getResults(driver);

            return banks;
        }
    }
}
